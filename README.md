Welcome to Hickory Trace Village Luxury Apartments! Hickory Trace Village offers more than a home…a destination and maintenance free living with a large variety of spacious floor plans that are set in a beautiful landscape, just minutes from downtown.

Address: 1250 5th Street NE, Hickory, NC 28601, USA

Phone: 828-328-5560
